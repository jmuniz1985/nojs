const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProductSchema = new Schema ({
    imagePath: {
    type: String,
    required: [true, "Need Product name"]
  },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
});

const Product = mongoose.model("Product", ProductSchema);
module.exports = Product;



//var ProductSchema = new Schema ({
  //  imagePath: {type: String, required: true},
    //title: {type: String, required: true},
    //description: {type: String, required: true},
    //price: {type: Number, required: true},
//});


